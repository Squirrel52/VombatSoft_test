$(function(){
    SyntaxHighlighter.all();
});
$(window).load(function(){
    $('.flexslider').flexslider({
        animation: "slide",
        start: function(slider){
            $('body').removeClass('loading');
        }
    });
    $(document).ready(function(){
        $('.mobile-button').click(function() {
            $('#decorations').hide();
            $('#m_decorations').hide();
            $('#catalog').hide();
            $('#main-menu').slideToggle( "slow" );
        });
        $('#catalog-list').click(function() {
            $('#decorations').hide();
            $('#m_decorations').hide();
            $('#catalog').slideToggle( "slow" );
        });
        $('#wedding_decorations').click(function(e) {
            $('#m_decorations').hide();
            $('#decorations').slideToggle( "slow" );
        });
        $('#men_decorations').click(function() {
            $('#decorations').hide();
            $('#m_decorations').slideToggle( "slow" );
        });
    });
});